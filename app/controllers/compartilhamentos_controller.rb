class CompartilhamentosController < ApplicationController
  before_action :authenticate_usuario!
  load_and_authorize_resource
  before_action :set_compartilhamento, only: [:show, :edit, :update, :destroy]
  before_action :busca_listas, only: [:new, :edit, :create, :update]

  respond_to :html

  def index
    @compartilhamentos = Compartilhamento.where(:usuario_id => current_user.id)
    @compartilhamentos_comigo = current_usuario.compartilhamentos_recebidos
    respond_with(@compartilhamentos)
  end

  def show
    respond_with(@compartilhamento)
  end

  def busca_listas
    @atracoes = Atracao.where(:usuario_id => current_user.id)
    @usuarios = Usuario.where.not(:id => current_user.id)
  end

  def new
    @compartilhamento = Compartilhamento.new
    @compartilhamento.atracao_id = params[:atracao_id] if params[:atracao_id]
    @compartilhamento.compartilhamentos_usuarios.build
    respond_with(@compartilhamento)
  end

  def edit
  end

  def create
    @compartilhamento = Compartilhamento.new(compartilhamento_params)
    @compartilhamento.usuario_id = current_user.id
    @compartilhamento.save
    respond_with(@compartilhamento)
  end

  def update
    @compartilhamento.update(compartilhamento_params)
    respond_with(@compartilhamento)
  end

  def destroy
    @compartilhamento.destroy
    respond_with(@compartilhamento)
  end

  def marcar_compartilhada_assistida
    @compartilhamento.compartilhamentos_usuarios.find_by(:usuario_id => current_usuario.id).update(:assistida => true)
    redirect_to root_path
  end

  private
  def set_compartilhamento
    @compartilhamento = Compartilhamento.find(params[:id])
  end

  def compartilhamento_params
    params.require(:compartilhamento).permit(:usuario_id, :atracao_id, :comentario, :compartilhamentos_usuarios_attributes => [:id, :usuario_id, :_destroy])
  end
end

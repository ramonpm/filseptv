require 'rails_helper'

RSpec.describe Usuario, :type => :model do
  describe 'validacoes' do
    it 'nome deve ser preenchido' do
      usuario = Usuario.create
      assert usuario.errors[:nome].present?, usuario.errors[:nome]
    end

    it 'email deve ser preenchido' do
      usuario = Usuario.create
      assert usuario.errors[:email].present?, usuario.errors[:email]
    end

    it 'senha deve ser preenchida' do
      usuario = Usuario.create
      assert usuario.errors[:password].present?, usuario.errors[:password]
    end
  end
end

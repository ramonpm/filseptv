class CreateCompartilhamentoUsuarios < ActiveRecord::Migration
  def change
    create_table :compartilhamentos_usuarios do |t|
      t.integer :compartilhamento_id, null: false
      t.integer :usuario_id, null: false
      t.boolean :assistida, null: false, default: false

      t.timestamps null: false
    end
    add_foreign_key :compartilhamentos_usuarios, :compartilhamentos
    add_foreign_key :compartilhamentos_usuarios, :usuarios
  end
end

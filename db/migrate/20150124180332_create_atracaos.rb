class CreateAtracaos < ActiveRecord::Migration
  def change
    create_table :atracoes do |t|
      t.integer :usuario_id, null: false
      t.string :nome, null: false
      t.string :midia, null: false
      t.datetime :exibicao
      t.integer :tipo, null: false
      t.boolean :assistida, null: false, default: false

      t.timestamps null: false
    end

    add_foreign_key :atracoes, :usuarios
  end
end

require "rails_helper"

RSpec.describe CompartilhamentosController, :type => :routing do
  describe "routing" do

    it "routes to #index" do
      expect(:get => "/compartilhamentos").to route_to("compartilhamentos#index")
    end

    it "routes to #new" do
      expect(:get => "/compartilhamentos/new").to route_to("compartilhamentos#new")
    end

    it "routes to #show" do
      expect(:get => "/compartilhamentos/1").to route_to("compartilhamentos#show", :id => "1")
    end

    it "routes to #edit" do
      expect(:get => "/compartilhamentos/1/edit").to route_to("compartilhamentos#edit", :id => "1")
    end

    it "routes to #create" do
      expect(:post => "/compartilhamentos").to route_to("compartilhamentos#create")
    end

    it "routes to #update" do
      expect(:put => "/compartilhamentos/1").to route_to("compartilhamentos#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/compartilhamentos/1").to route_to("compartilhamentos#destroy", :id => "1")
    end

  end
end

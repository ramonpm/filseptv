require 'rails_helper'

RSpec.describe "compartilhamentos/edit", :type => :view do
  before(:each) do
    @compartilhamento = assign(:compartilhamento, Compartilhamento.create!(
      :usuario_id => 1,
      :comentario => "MyString"
    ))
  end

  it "renders the edit compartilhamento form" do
    render

    assert_select "form[action=?][method=?]", compartilhamento_path(@compartilhamento), "post" do

      assert_select "input#compartilhamento_usuario_id[name=?]", "compartilhamento[usuario_id]"

      assert_select "input#compartilhamento_comentario[name=?]", "compartilhamento[comentario]"
    end
  end
end

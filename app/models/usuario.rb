class Usuario < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  has_many :atracaos
  has_many :compartilhamentos
  has_many :compartilhamentos_usuarios, :class_name => 'CompartilhamentoUsuario', :foreign_key => :compartilhamento_id
  has_and_belongs_to_many :compartilhamentos_recebidos, class_name: 'Compartilhamento'

  validates_presence_of :nome

  before_save :setar_admin, if: :sem_usuarios?

  def admin_to_s
    valor = self.admin
    if valor
      'Sim'
    else
      'Não'
    end
  end

  private
  def sem_usuarios?
    Usuario.all.empty?
  end

  def setar_admin
    self.admin = true
  end

end

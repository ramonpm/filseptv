require 'rails_helper'

RSpec.describe "atracaos/show", :type => :view do
  before(:each) do
    @atracao = assign(:atracao, Atracao.create!(
      :usuario_id => 1,
      :nome => "Nome",
      :midia => "Midia"
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(/1/)
    expect(rendered).to match(/Nome/)
    expect(rendered).to match(/Midia/)
  end
end

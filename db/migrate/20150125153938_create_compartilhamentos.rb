class CreateCompartilhamentos < ActiveRecord::Migration
  def change
    create_table :compartilhamentos do |t|
      t.integer :usuario_id, null: false
      t.integer :atracao_id, null: false
      t.string :comentario

      t.timestamps null: false
    end

    add_foreign_key :compartilhamentos, :usuarios
    add_foreign_key :compartilhamentos, :atracoes, column: :atracao_id
  end
end

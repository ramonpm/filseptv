require 'rails_helper'

RSpec.describe "compartilhamentos/show", :type => :view do
  before(:each) do
    @compartilhamento = assign(:compartilhamento, Compartilhamento.create!(
      :usuario_id => 1,
      :comentario => "Comentario"
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(/1/)
    expect(rendered).to match(/Comentario/)
  end
end

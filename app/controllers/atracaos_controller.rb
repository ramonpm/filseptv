class AtracaosController < ApplicationController
  before_action :authenticate_usuario!
  load_and_authorize_resource
  before_action :set_atracao, only: [:show, :edit, :update, :destroy, :marcar_assistida]

  respond_to :html

  def inicio
    @atracaos = Atracao.where(:usuario_id => current_user.id, :assistida => false).order(:exibicao)
    @compartilhamentos = current_usuario.compartilhamentos_recebidos.where('compartilhamentos_usuarios.assistida is false')
  end

  def index
    @atracaos = Atracao.where(:usuario_id => current_user.id).order(:assistida, :exibicao)
    respond_with(@atracaos)
  end

  def show
    respond_with(@atracao)
  end

  def new
    @atracao = Atracao.new
    respond_with(@atracao)
  end

  def edit
  end

  def create
    @atracao = Atracao.new(atracao_params)
    @atracao.usuario_id = current_user.id
    respond_to do |format|
      if @atracao.save
        flash[:notice] = "Atração #{@atracao.nome} criada com sucesso."
        format.html { redirect_to(root_path) }
        format.xml { render xml: @atracao }
      else
        format.html { render action: "new" }
        format.xml { render xml: @atracao }
      end
    end
  end

  def update
    @atracao.update(atracao_params)
    respond_with(@atracao)
  end

  def destroy
    @atracao.destroy
    respond_with(@atracao)
  end

  def explorar
    @usuarios = Usuario.where.not(:id => current_user.id)
  end

  def atracoes_do_usuario
    @usuario = Usuario.find(params[:usuario_id])
    @atracoes = @usuario.atracaos
  end

  def marcar_assistida
    @atracao.update(:assistida => true)
    redirect_to root_path
  end

  private
  def set_atracao
    @atracao = Atracao.find(params[:id])
  end

  def atracao_params
    params.require(:atracao).permit(:nome, :midia, :exibicao, :tipo)
  end
end

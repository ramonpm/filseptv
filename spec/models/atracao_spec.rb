require 'rails_helper'

RSpec.describe Atracao, :type => :model do
  describe 'validacoes' do
    it 'nome obrigatorio' do
      atracao = Atracao.create
      expect(atracao.errors[:nome]).to be_truthy
    end

    it 'midia obrigatoria' do
      atracao = Atracao.create
      expect(atracao.errors[:midia]).to be_truthy
    end
  end
end

class Atracao < ActiveRecord::Base

  self.table_name = 'atracoes'

  TIPO_FILME = {:tipo => 1, :label => 'Filme'}
  TIPO_PROGRAMA_TV = {:tipo => 2, :label => 'Programa de TV'}
  TIPO_SERIE = {:tipo => 3, :label => 'Série'}
  TIPOS = [TIPO_FILME, TIPO_PROGRAMA_TV, TIPO_SERIE]

  belongs_to :usuario
  has_many :compartilhamentos, :through => :atracoes_compartilhadas

  validates_presence_of :usuario_id, :nome, :midia, :tipo
  validate :formato_exibicao

  def exibicao_dt_picker
    if self.exibicao.present?
      self.exibicao.strftime("%d/%m/%Y %H:%M")
    end
  end

  def assistida?
    if self.assistida
      'Sim'
    else
      'Não'
    end
  end

  private

  def formato_exibicao
    if self.exibicao.present?
      self.errors.add(:exibicao, 'não está em um formato válido, use o seletor...') unless DateTime.parse(self.exibicao.to_s)
    end
  end

end

require 'rails_helper'

RSpec.describe "atracaos/new", :type => :view do
  before(:each) do
    assign(:atracao, Atracao.new(
      :usuario_id => 1,
      :nome => "MyString",
      :midia => "MyString"
    ))
  end

  it "renders new atracao form" do
    render

    assert_select "form[action=?][method=?]", atracaos_path, "post" do

      assert_select "input#atracao_usuario_id[name=?]", "atracao[usuario_id]"

      assert_select "input#atracao_nome[name=?]", "atracao[nome]"

      assert_select "input#atracao_midia[name=?]", "atracao[midia]"
    end
  end
end

class CreateUsuarios < ActiveRecord::Migration
  def change
    create_table :usuarios do |t|
      t.string :nome, null: false
      t.boolean :admin, null: false, default: false

      t.timestamps null: false
    end
  end
end

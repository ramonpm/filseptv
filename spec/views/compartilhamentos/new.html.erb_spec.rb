require 'rails_helper'

RSpec.describe "compartilhamentos/new", :type => :view do
  before(:each) do
    assign(:compartilhamento, Compartilhamento.new(
      :usuario_id => 1,
      :comentario => "MyString"
    ))
  end

  it "renders new compartilhamento form" do
    render

    assert_select "form[action=?][method=?]", compartilhamentos_path, "post" do

      assert_select "input#compartilhamento_usuario_id[name=?]", "compartilhamento[usuario_id]"

      assert_select "input#compartilhamento_comentario[name=?]", "compartilhamento[comentario]"
    end
  end
end

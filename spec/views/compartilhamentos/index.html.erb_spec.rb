require 'rails_helper'

RSpec.describe "compartilhamentos/index", :type => :view do
  before(:each) do
    assign(:compartilhamentos, [
      Compartilhamento.create!(
        :usuario_id => 1,
        :comentario => "Comentario"
      ),
      Compartilhamento.create!(
        :usuario_id => 1,
        :comentario => "Comentario"
      )
    ])
  end

  it "renders a list of compartilhamentos" do
    render
    assert_select "tr>td", :text => 1.to_s, :count => 2
    assert_select "tr>td", :text => "Comentario".to_s, :count => 2
  end
end

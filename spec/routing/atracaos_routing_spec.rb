require "rails_helper"

RSpec.describe AtracaosController, :type => :routing do
  describe "routing" do

    it "routes to #index" do
      expect(:get => "/atracaos").to route_to("atracaos#index")
    end

    it "routes to #new" do
      expect(:get => "/atracaos/new").to route_to("atracaos#new")
    end

    it "routes to #show" do
      expect(:get => "/atracaos/1").to route_to("atracaos#show", :id => "1")
    end

    it "routes to #edit" do
      expect(:get => "/atracaos/1/edit").to route_to("atracaos#edit", :id => "1")
    end

    it "routes to #create" do
      expect(:post => "/atracaos").to route_to("atracaos#create")
    end

    it "routes to #update" do
      expect(:put => "/atracaos/1").to route_to("atracaos#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/atracaos/1").to route_to("atracaos#destroy", :id => "1")
    end

  end
end

json.array!(@compartilhamentos) do |compartilhamento|
  json.extract! compartilhamento, :id, :usuario_id, :comentario
  json.url compartilhamento_url(compartilhamento, format: :json)
end

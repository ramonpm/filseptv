class Compartilhamento < ActiveRecord::Base

  belongs_to :usuario
  belongs_to :atracao
  has_many :compartilhamentos_usuarios, :class_name => 'CompartilhamentoUsuario', :foreign_key => :compartilhamento_id
  accepts_nested_attributes_for :compartilhamentos_usuarios, :allow_destroy => true
  has_and_belongs_to_many :usuarios_compartilhados, :class_name => 'Usuario'

  validates_presence_of :atracao_id, :usuario_id

  def compartilhado_com
    usuarios = ''
    self.usuarios_compartilhados.each_with_index do |usuario, index|
      usuarios += usuario.nome
      usuarios += ', ' if index < (self.usuarios_compartilhados.size - 1)
    end
    usuarios
  end
end

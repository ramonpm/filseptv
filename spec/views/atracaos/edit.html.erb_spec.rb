require 'rails_helper'

RSpec.describe "atracaos/edit", :type => :view do
  before(:each) do
    @atracao = assign(:atracao, Atracao.create!(
      :usuario_id => 1,
      :nome => "MyString",
      :midia => "MyString"
    ))
  end

  it "renders the edit atracao form" do
    render

    assert_select "form[action=?][method=?]", atracao_path(@atracao), "post" do

      assert_select "input#atracao_usuario_id[name=?]", "atracao[usuario_id]"

      assert_select "input#atracao_nome[name=?]", "atracao[nome]"

      assert_select "input#atracao_midia[name=?]", "atracao[midia]"
    end
  end
end

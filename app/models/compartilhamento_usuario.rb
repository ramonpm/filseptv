class CompartilhamentoUsuario < ActiveRecord::Base

  self.table_name = 'compartilhamentos_usuarios'

  belongs_to :compartilhamento
  belongs_to :usuario

  validates_presence_of :usuario_id

  def assistida?
    if self.assistida
      'Sim'
    else
      'Não'
    end
  end
end
